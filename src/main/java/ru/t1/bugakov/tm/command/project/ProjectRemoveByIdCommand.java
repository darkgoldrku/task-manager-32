package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectEndpoint().removeProjectById(new ProjectRemoveByIdRequest(id));
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

}
