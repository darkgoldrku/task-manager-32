package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataXmlSaveJaxBRequest;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().dataXmlSaveJaxB(new DataXmlSaveJaxBRequest());
    }

    @Override
    public @NotNull
    String getName() {
        return "data-save-xml-jaxb";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Save data to xml file with jaxb";
    }

}
