package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataBase64LoadRequest;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        getDomainEndpoint().dataBase64Load(new DataBase64LoadRequest());
    }

    @Override
    public @NotNull
    String getName() {
        return NAME;
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from BASE64 file";
    }

}
