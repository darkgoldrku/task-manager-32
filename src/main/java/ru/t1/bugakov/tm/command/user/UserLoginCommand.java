package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        getAuthEndpoint().login(new UserLoginRequest(login, password));
    }

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Login user.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
